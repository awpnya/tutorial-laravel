<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    public function index()
        {
//            dd(auth()->user());
            $users = DB::table('users')->paginate(5);
            return view('index',['users' => $users]);
        }

    public function cari(Request $request)
    {
        $cari = $request->cari;

        $users = DB::table('users')
            ->where('email','like',"%".$cari."%")
            ->paginate(5);

        return view('index',['users' => $users]);
    }

    public function tambah()
    {
        return view('tambah');
    }

    public function store(Request $request)
    {
        $messages = [
            'required' => ':attribute wajib diisi cuy!!!',
            'min' => ':attribute harus diisi minimal :min karakter ya cuy!!!',
            'max' => ':attribute harus diisi maksimal :max karakter ya cuy!!!',
        ];

        $validate = $this->validate($request,[
            'name' => 'required|min:5|max:20',
            'email' => 'required|email',
            'password' => 'required'
        ],$messages);

        DB::table('users')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/users');
    }
    public function edit($id)
    {
        $users = DB::table('users')->where('id',$id)->get();
        // passing data pegawai yang didapat ke view edit.blade.php
        return view('edit',['users' => $users]);
    }
    // update data
    public function update(Request $request)
    {
        // update data
        DB::table('users')->where('id',$request->id)->update([
            'name' => $request->name,
            'email' => $request->email
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/users');
    }
    public function hapus($id)
    {
        //validasi hapus user sendiri + flash message
        if (auth()->user()->id == $id) {
            return redirect('/users')->with('status', 'Eror!');
        }

        DB::table('users')->where('id',$id)->delete();

        return redirect('/users');
    }

}
