<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    //$collection = collect([1,2,3]);
    //dd($collection);
        return view('welcome');
    //$users = \App\User::all();
    //dd($users);
});

Route::get('halo', function () {
    return "Halo selamat datang di web saya";
});

Route::get('blog', function () {
    return view('blog');
});
//route CRUD
Route::get('/users','UsersController@index');
Route::get('/users/cari','UsersController@cari');
Route::get('/users/tambah','UsersController@tambah');
Route::post('/users/store','UsersController@store');
Route::get('/users/edit/{id}','UsersController@edit');
Route::post('/users/update','UsersController@update');
Route::get('/users/hapus/{id}','UsersController@hapus');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
