<!DOCTYPE html>
<html>
<head>
    <title>Persiapan Magang Javan </title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">

</head>
<body>

<h2><a>Laravel SCRUD</a></h2>
<h3>Data Pengguna</h3>

<a href="/users"> Kembali</a>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<br/>
<br/>

<form action="/users/store" method="post">
    {{ csrf_field() }}
    Nama <br> <input type="text" name="name" required="required" value="{{old('name')}}"> <br/>
    Email <br> <input type="email" name="email" required="required" value="{{old('email')}}"> <br/>
    Password <br> <input type="password" name="password" required="required " value="{{old('password')}}"> <br/>
    <br>
    <input type="submit" value="Simpan Data">

</form>



</body>
</html>
