<!DOCTYPE html>
<html>
<head>
    <title>Persiapan Magang Javan </title>
{{--   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css">--}}
{{--  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/form.css">--}}
{{--   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/form.min.css">--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.css" rel="stylesheet" type="text/css"> </link>

</head>
<body>
    <div class="ui container">
        <h2>Laravel SCRUD</h2>
        <h3>Data Pengguna</h3>
        <a href="/home"><- Kembali </a>

        <form action="/users/cari" method="GET">
            <input type="text" name="cari" placeholder="menggunakan email" value="{{ old('cari') }}">
            <input type="submit" value="Search">
        </form>

       @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif

        <br>
        <table border="1">
            <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>None</th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        <a href="/users/tambah">Tambah</a>
                        |
                        <a href="/users/edit/{{ $user->id }}">Edit</a>
                        |
                        <a href="/users/hapus/{{ $user->id }}">Hapus</a>
                    </td>
                </tr>
            </body>
            @endforeach

            <tfoot>
            <tr><th colspan="">
                    <div class="ui right floated pagination menu">
                        {{--                                        <a class="icon item">--}}
                        {{--                                            Halaman : {{ $pegawai->currentPage() }} <br/>--}}
                        {{--                                            Jumlah Data : {{ $pegawai->total() }} <br/>--}}
                        {{--                                            Data Per Halaman : {{ $pegawai->perPage() }} <br/>--}}
                        {{--                                        </a>--}}

                        {{ $users->links() }}

                    </div>
                </th>
            </tr></tfoot>
        </table>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.js"> </script>
</html>
